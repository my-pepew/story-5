from django.contrib import admin
from .models import Schedule, Task

# Register your models here.
admin.site.register(Schedule)
admin.site.register(Task)