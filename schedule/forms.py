from django import forms

class ScheduleForm(forms.Form):
    name = forms.CharField()
    lecturer = forms.CharField()
    credits = forms.IntegerField()
    description = forms.CharField()
    period = forms.CharField()
    classroom = forms.CharField()