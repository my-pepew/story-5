from django.shortcuts import render, redirect
from .models import Schedule, Task
from .forms import ScheduleForm

# Create your views here.
def view_schedules(request):
    ctx = {}
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            sch = Schedule(name=form.data.get('name'),lecturer=form.data.get('lecturer'),credits=form.data.get('credits'),description=form.data.get('description'),period=form.data.get('period'),classroom=form.data.get('classroom'))
            sch.save()
            return redirect('view_schedule')
    else:
        form = ScheduleForm()
        ctx['form'] = form
        ctx['datas'] = Schedule.objects.all()
    return render(request, 'schedule_view.html', ctx)

def delete_schedule(request, sch_id):
    try:
        sch = Schedule.objects.get(pk=sch_id)
    except:
        return redirect('view_schedule')
    
    sch.delete()
    return redirect('view_schedule')

def schedule_detail(request, sch_id):
    ctx = {}
    try:
        sch = Schedule.objects.get(pk=sch_id)
        ctx['schedule'] = sch
    except:
        return redirect('view_schedule')

    return render(request, 'schedule_detail.html', ctx)

def tasks_view(request, sch_id):
    ctx = {}
    try:
        sch = Schedule.objects.get(pk=sch_id)
        tasks = Task.objects.filter(subject_id=sch.schedule_id).order_by('due_date')

        ctx['tasks'] = tasks
        ctx['subject'] = sch
    except Exception as e:
        print(e)
        return redirect('view_schedule')

    return render(request, 'tasks.html', ctx)