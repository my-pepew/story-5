from django.urls import path
from . import views

urlpatterns = [
    path('schedule', views.view_schedules, name='view_schedule'),
    path('schedule/delete/<int:sch_id>', views.delete_schedule, name='delete_schedule'),
    path('schedule/<int:sch_id>', views.schedule_detail, name='schedule_detail'),
    path('tasks/<int:sch_id>', views.tasks_view, name='tasks_view'),
]
